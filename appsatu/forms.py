from django import forms



class ScheduleForm(forms.Form):

    error_messages = {
            'required': 'Please fill this field!',
            'invalid': 'Fill it with your email!',
        }

    name_attrs = {
        'class' : 'form-control',
        'placeholder': 'Tell me your activity!',
    }

    day_attrs = {
        'class' : 'form-control',
        'placeholder': 'What day is it?',
    }
    date_attrs = {
        'type':'datetime-local', 
        'class':'form-control'
    }

    place_attrs = {
        'class' : 'form-control',
        'placeholder': 'Where is the place?',
    }
    category_attrs = {
        'class' : 'form-control',
        'placeholder': 'What is the category?',
    }


    activityname = forms.CharField(label='Activity Name', required=True, max_length=50, empty_value='Anonymous', widget=forms.TextInput(attrs=name_attrs))
    day = forms.CharField(label='Day', required=True, max_length=50, widget=forms.TextInput(attrs=day_attrs))
    date = forms.DateTimeField(label='Date', required=True,  widget=forms.DateInput(attrs=date_attrs))
    place = forms.CharField(label='Place', required=True, max_length=50, empty_value='Please fill the place!', widget=forms.TextInput(attrs=place_attrs))
    category = forms.CharField(label='Category', required=True, max_length=50, empty_value='Please fill the category!', widget=forms.TextInput(attrs=category_attrs))