from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from datetime import datetime, date
from .models import Schedule
from .forms import ScheduleForm

# Create your views here.
response = {'author' : "Alya Isti Safira"}

curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2000, 6, 23)
entry_date = date(2017, 7, 31)
degreenow = "Undergraduate student"
degreelater = "Bachelor of Computer Science"

def calculate_degree(entry_year):
    if (curr_year - entry_year) < 4:
        return degreenow
    else :
        return degreelater

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
    
def iniweb(request):
    args = {'myAge': calculate_age(birth_date.year), 'myDegree': calculate_degree(entry_date.year) }
    return render(request, 'appsatu/story3-ppw.html', args)

def iniweb2(request):
    return render(request, 'appsatu/story3-ppw-projects.html', {})

def iniweb3(request):
    return render(request, 'appsatu/story3-ppw-form.html', {})


#baruuuu
def index(request):
    response['message_post'] = ScheduleForm
    return render(request, 'appsatu/form_field.html' , response)


def message_post(request):
    #form = ScheduleForm(request.POST or None)
    if(request.method == 'POST'):
        response['activityname'] = request.POST['activityname'] 
        response['day'] = request.POST['day']
        response['date'] = request.POST['date']
        response['place'] = request.POST['place']
        response['category'] = request.POST['category']
        the_sched = Schedule(activityname=response['activityname'], day=response['day'], date=response['date'], place=response['place'], category=response['category'])
        the_sched.save()
        return redirect('table')

    else:
        return HttpResponseRedirect('schedule')

def message_table(request):
    tablesched = Schedule.objects.all()
    response['tablesched'] = tablesched
    return render(request, 'appsatu/form_result.html', response)

def delete_table(request):
    deletetablesched = Schedule.objects.all().delete()
    response['deletetablesched'] = deletetablesched
    return render(request, 'appsatu/form_result.html', response)
