from django.conf.urls import url
from django.urls import path
from . import views
from .views import iniweb, iniweb2, iniweb3, index, message_post, message_table, delete_table


urlpatterns = [
    path('', iniweb, name='iniweb'),
    path('', iniweb2, name='iniweb2'),
    path('', iniweb3, name='iniweb3'),
    path('', index, name='index'),
    path('', message_post, name='message_post'),
    path('', message_table, name='message_table'),
    path('', delete_table, name='delete_table'),
]
