"""websaya URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from appsatu.views import iniweb as aboutme
from appsatu.views import iniweb2 as projects
from appsatu.views import iniweb3 as guestpage
from appsatu.views import index as schedule
from appsatu.views import message_post as post
from appsatu.views import message_table as table
from appsatu.views import delete_table as delete
from django.urls import path, include, re_path
from django.conf.urls import url, include


urlpatterns = [
    path('admin/', admin.site.urls),
    #path('', include('appsatu.urls')),
    re_path(r'^$', aboutme, name="aboutme"),
    re_path(r'^projects', projects, name="projects"),
    re_path(r'^guestpage', guestpage, name="guestpage"),
    re_path(r'^schedule', schedule, name="schedule"),
    re_path(r'^table', table, name="table"),
    re_path(r'^post', post, name="post"),
    re_path(r'^delete', delete, name="delete"),
    
]
